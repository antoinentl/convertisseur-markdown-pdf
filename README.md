# Convertisseur Markdown à PDF

> Utilisation de l’intégration continue de Gitlab pour exécuter une conversion de Markdown à PDF en ayant la possibilité de styler le résultat avec des CSS.

Cet outil est fait pour être utilisé de 3 manières différentes :

1. en effectuant une `merge-request` sur ce dépôt sans rien avoir à installer sur votre machine et sans avoir de connaissance technique particulière ;
2. en faisant un `fork` du projet sur votre propre compte ce qui permet de le personnaliser (et accessoirement de ne pas utiliser la CI de mon compte) ;
3. en l’installant localement pour faire des conversions locales, cela requiert de savoir installer des outils sur sa machine.

Ces 3 usages sont détaillés ci-après :

## 1. `merge-request` sur ce dépôt (sans installation)

1. Éditer le fichier `source.md` avec le contenu de votre markdown, [lien direct](./../../../-/edit/main/source.md) ;
2. Commiter les changements avec un message explicite et faire une `merge-request` ;
3. Attendre que le `Pipeline` passe pour la `merge-request`, il devrait finir par afficher un ✅ ;
4. Cliquer sur l’icône `Artifacts` qui apparait à droite dans le bloc de Pipeline, ça ressemble à une flèche vers le bas qui arrive dans un bac ;
5. Cliquer dans la petite fenêtre qui s’est ouverte sur `generate:archive` en dessous de « Download artifacts ».

Normalement, cela devrait télécharger le fichier PDF généré à partir de votre contenu markdown 🎉

Si vous avez peur de vous tromper, il y a un exemple avec cette `merge-request` :
https://gitlab.com/davidbgk/convertisseur-markdown-pdf/-/merge_requests/1

⚠️ Il n’y a aucune garantie que le résultat d’aujourd’hui soit le même que demain, le projet et son rendu des PDF vont évoluer !

Note : si le rendu final n’est pas terrible, il est possible de modifier le fichier `style.css` à la racine du dépôt pour affiner cela (un peu plus technique). Si le résultat n’est pas conforme à vos attentes, indiquez-le en commentaire et on essaye de voir ensemble.

Un exemple de `merge-request` qui contient un style particulier pour les citations :
https://gitlab.com/davidbgk/convertisseur-markdown-pdf/-/merge_requests/2


## 2. `fork` du projet

Je ne sais pas encore combien de minutes de CI je vais pouvoir allouer à ce projet, si ça atteint la limite il devra s’arrêter pour les personnes non-techniques. Aussi, j’encourage les personnes qui sont en capacité de le faire d’héberger leur propre dépôt sur leur propre compte afin que le décompte s’effectue de manière distribuée.

1. Aller à la racine du dépôt https://gitlab.com/davidbgk/convertisseur-markdown-pdf ;
2. Cliquer sur l’icône `Fork` en haut à droite ;
3. Se laisser guider, la procédure évoluant au cours du temps je préfère ne pas donner d’infos trop précises/obsolètes.

Ensuite, vous pouvez proposer à des personnes de faire des `merge-request` chez vous, vous pouvez proposer une CSS par défaut qui soit à vos couleurs/préférences, vous pouvez corriger les bugs et les faire remonter ici, etc.


## 3. installation locale

Cela requiert quelques compétences techniques mais ça vous permet aussi de générer autant de fichiers PDF que vous le souhaitez sans être dépendant·e de Gitlab.

### Installation

Pré-requis : Python3 et Pango (cf. la [doc d’installation de weasyprint](https://doc.courtbouillon.org/weasyprint/stable/first_steps.html#installation)).

Installer et activer un environnement virtuel :

    $ python3 -m venv venv
    $ source venv/bin/activate

Installer les dépendances :

    $ make install


### Génération locale

Lancer la commande :

    $ make pdf


## Légalité

Ce dépôt est sous licence MIT.

Les documents soumis lors des `merge-requests` et les artefacts produits sont la propriété (et sous la responsabilité !) de leurs auteur·ices respectif·ves.
